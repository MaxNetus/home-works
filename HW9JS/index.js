// Створити новий HTML тег на сторінці можна за допомогою createElement(''), де всередині лапок ми запишимо тег який ми хочемо створити.  Наприклад const newEl = createElement('div'); 
// Перший параметр функції insertAdjacentHTML означає місце куди ми будемо цей html вставляти відповідно до елемента. Можливі варіанти: beforebegin, afterbegin, beforeend та afterend. 
// Видалити елемент зі сторінки можна за допомогою мутоду remove(). Синтаксис: lement.remove().

"use strict"

const div = document.createElement('div');
document.body.append(div);

function getList(list, parent = document.body) {
  let ul = document.createElement('ul');
  list.map(el => {
    if (Array.isArray(el)) {
      getList(el, ul);
    } else {
      let li = document.createElement('li');
      li.textContent = el;
      ul.append(li);
    }
  });
  parent.append(ul);
};

function crearList() {
  document.body.innerHTML = '';
}
setTimeout(() => crearList(), 3000);


getList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], div);
getList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);




















