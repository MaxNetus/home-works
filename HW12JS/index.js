// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Тому що при копіюванні та вставці в input тексту, наша перевірка не спрацює. В такому випадку краще вішати addEventListener на мишку(клік, фокус)

"use strict"

const btn = document.querySelectorAll('.btn');
const array = Array.from(btn).map(el => el.id);

document.addEventListener('keydown', function (ev) {
  if (array.includes(ev.code)) {
    btn.forEach(i => i.classList.remove('color'));
    document.getElementById(ev.code).classList.add('color');
  }
});




