import { Component } from "react";
import { Favorites } from "../favorites/Favorites";

import { Cart } from "../cart/Cart";
import PropTypes from "prop-types";

export class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <nav className="navbar bg-primary border-bottom " >
                    <div className="container">
                        <h1 className="navbar-brand fs-2">Plastic Military Planes</h1>
                        <div className="d-flex gap-3">
                            <Favorites favItemsCount={this.props.favItemsCount} />
                            <Cart cartItemsCount={this.props.cartItemsCount} />
                        </div>
                    </div>
                </nav>
            </>
        );
    }
}

Header.propTypes = {
    cartItemsCount: PropTypes.number,
    favItemsCount: PropTypes.number,
};

Header.defaultProps = {
    cartItemsCount: 0,
    favItemsCount: 0,
};
