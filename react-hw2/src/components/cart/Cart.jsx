import { Component } from "react";
import PropTypes from "prop-types";

export class Cart extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <button type="button" className="btn bg-primary-subtle btn-lg position-relative pt-0">
                <img width={25} height={25} src="./img/shopping-cart.png" alt="shopping-cart" />
                <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-success">
          {this.props.cartItemsCount}
        </span>
            </button>
        );
    }
}

Cart.propTypes = {
    cartItemsCount: PropTypes.number,
};
