import { Component } from "react";
import PropTypes from "prop-types";

import 'bootstrap/dist/css/bootstrap.min.css';

export class ItemCard extends Component {
    constructor(props) {
        super(props);
    }
    favBtnClick = (e, id) => {
        this.props.favourite ? this.props.removeFromFavClick(e, id) : this.props.addToFavClick(e, id)
    };
    render() {
        const { id, name, price, img, article, color } = this.props.item;
        return (
            <>
                <li id={id} className="card bg-secondary-subtle" style={{ width: "350px" }}>
                    <div className="card-body d-flex flex-column justify-content-between">
                        <h3 className="card-title">{name}</h3>
                        <div className="d-flex justify-content-between">
                            <button onClick={(e) => this.favBtnClick(e, id)} className="btn border-0 p-0">
                                <img
                                    width={30}
                                    height={30}
                                    src={this.props.favourite ? "./img/star-active.png" : "./img/star.png"}
                                    alt="favorite-star"
                                />
                            </button>
                            <button onClick={(e) => this.props.openToCartModal(e, id)} className="btn btn-primary">
                                Add to cart
                            </button>
                        </div>
                        <p className="card-text">{"Article: " + article}</p>
                        <p className="card-text">{"Color: " + color}</p>
                        <p className="card-text fw-bold">{price + "₴"}</p>
                        <img src={img} className="card-img-top p-2" alt="item-img" />
                    </div>
                </li>
            </>
        );
    }
}

ItemCard.propTypes = {
    addToFavClick: PropTypes.func,
    openToCartModal: PropTypes.func,
    removeFromFavClick: PropTypes.func,
    favourite: PropTypes.bool,
    item: PropTypes.object,
};
