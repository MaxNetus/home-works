import { Component } from "react";
import PropTypes from "prop-types";
import "./modal.scss";

export class Modal extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { name, price, img } = this.props.selectedItem;
        return (
            <>
                <div
                    className="d-flex flex-column text-center rounded p-4 pt-3 position-fixed top-50 start-50 translate-middle bg-light fs-4"
                    style={{ zIndex: 2, width: "350px" }}
                >
                    <div className="d-flex justify-content-between align-items-center mb-2">
                        <p className="m-0 fs-5">Adding this item to a cart:</p>
                        <button onClick={this.props.closeToCartModal} className="btn p-0 fw-bold fs-4 border-0">
                            &#10005;
                        </button>
                    </div>
                    <p>{name}</p>
                    <img src={img} width={"100%"} alt="adding-item" />
                    <p className="fw-bold mt-2">{price + " UAH"}</p>
                    <button
                        onClick={() => {
                            this.props.addToCart(this.props.selectedItem);
                            this.props.closeToCartModal();
                        }}
                        className="btn btn-primary w-100"
                    >
                        Add
                    </button>
                </div>
                <div className="modalBg" onClick={this.props.closeToCartModal}></div>
            </>
        );
    }
}

Modal.propTypes = {
    addToCart: PropTypes.func,
    selectedItem: PropTypes.object,
    closeToCartModal: PropTypes.func,
};
