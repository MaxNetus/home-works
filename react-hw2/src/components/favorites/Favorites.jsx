import { Component } from "react";
import PropTypes from "prop-types";

export class Favorites extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <button type="button" className="btn btn-secondary btn-lg position-relative pt-0">
                <img width={25} height={25} src="./img/star-active.png" alt="favorite-star" />
                <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-warning">
          {this.props.favItemsCount}
        </span>
            </button>
        );
    }
}

Favorites.propTypes = {
    favItemsCount: PropTypes.number,
};
