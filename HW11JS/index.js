
"use strict"


const form = document.querySelector('.password-form');

const showPas = document.querySelector('.newPasShow');
const hidIcon = document.querySelector('.hidden');

const confPasShow = document.querySelector('.confPasShow');
const confPasHiden = document.querySelector('.confPasHiden');

const newPas = document.querySelector('.newPas');
const confPas = document.querySelector('.confPass');
const wrong = document.querySelector('.wrong');

form.addEventListener('click', function (ev) {
  if (ev.target.tagName === 'I') {
    if (ev.target.className.includes('newPasShow')) {
      newPas.removeAttribute('type', 'password');
      newPas.setAttribute('type', 'text');
      showPas.classList.add('noActive');
      hidIcon.classList.remove('hidden');
    }
    if (ev.target.className.includes('confPasShow')) {
      confPas.removeAttribute('type', 'password');
      confPas.setAttribute('type', 'text');
      confPasShow.classList.add('noActive');
      confPasHiden.classList.remove('confPasHiden');
    }
  }
  if (ev.target.tagName === 'BUTTON') {
    if (newPas.value === confPas.value) {
      alert('You are welcome')
      wrong.innerText = '';
    } else {
      wrong.innerText = 'Потрібно ввести однакові значення';
    }
    ev.preventDefault();
  }
})




