import { Routes, Route } from "react-router-dom";
import { Home, Cart, Favorites } from "./pages";

const AppRoutes = ({ items, favItems, cartItems, openModal, addToFav, removeFromFav }) => {
    return (
        <>
            <Routes>
                <Route
                    path="/"
                    element={
                        <Home
                            openModal={openModal}
                            addToFav={addToFav}
                            removeFromFav={removeFromFav}
                            items={items}
                            favItems={favItems}
                        />
                    }
                />
                <Route
                    path="/favorites"
                    element={
                        <Favorites
                            openModal={openModal}
                            addToFav={addToFav}
                            removeFromFav={removeFromFav}
                            items={favItems}
                            favItems={favItems}
                        />
                    }
                />
                <Route
                    path="/cart"
                    element={
                        <Cart addToFav={addToFav} removeFromFav={removeFromFav} openModal={openModal} cartItems={cartItems} />
                    }
                />
            </Routes>
        </>
    );
}

export { AppRoutes };
