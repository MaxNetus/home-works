import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

import {FavoritesBtn} from "../favBtn/FavBtn";
import {CartBtn} from "../cartBtn/CartBtn";

const MainLogo = (props) => {
    return (
        <>
            <img src={props.img} width="50" height="50"/>
            <span className="fs-2 fw-bold font-monospace text-decoration-underline">{props.slogan}</span>
        </>
    )
}

const Header = ({favCount, cartCount}) => {
    const favItems = useSelector((state) => state.favItems)
    const cartItems = useSelector((state) => state.cartItems)

    return (
        <>
            <nav className="navbar bg-primary-subtle">
                <div className="container">
                    <Link to={"/"} className="btn bg-transparent fw-semibold fs-2">
                        <MainLogo img={"./img/plane.png"} slogan={"AirMan"}/>
                    </Link>
                    <div className="d-flex gap-5">
                        <Link to={"/favorites"}>
                            <FavoritesBtn favCount={favItems.length}/>
                        </Link>
                        <Link to={"/cart"}>
                            <CartBtn cartCount={cartItems.length}/>
                        </Link>
                    </div>
                </div>
            </nav>
        </>
    );
}

export {Header}

