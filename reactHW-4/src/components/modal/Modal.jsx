import { useDispatch, useSelector } from "react-redux";

import { closeModal } from "../../redux/slices/modalSlice";
import { addToCart } from "../../redux/slices/cartItemsSlice";
import { removeFromCart } from "../../redux/slices/cartItemsSlice";

import "./modal.scss";

const Modal = () => {

    const dispatch = useDispatch();
    const selectedItem = useSelector((state) => state.selectedItem);
    const isRemoving = useSelector((state) => state.modal.isRemoving);
    const { name, price, img, article, color } = selectedItem;

    return (
        <>
            <div
                className="d-flex flex-column text-center rounded p-4 pt-3 position-fixed top-50 start-50 translate-middle bg-secondary-subtle fs-4"
                style={{ zIndex: 2, width: "400px" }}
            >
                <div className="d-flex justify-content-between align-items-center mb-2">
                    <p className="m-0 fs-5">{isRemoving ? `Remove from cart:` : `Add to a Cart:`}</p>
                    <button onClick={() => dispatch(closeModal())} className="btn p-0 fw-bold fs-4 border-0">
                        &#10005;
                    </button>
                </div>
                <h3>{name}</h3>
                <img src={img} width={"100%"} alt="plane" />
                <p className="fw-bold mt-2">{price + "₴"}</p>
                <p className="card-text">{"Article: " + article}</p>
                <p className="card-text">{"Color: " + color}</p>
                {isRemoving ? (
                    <button
                        onClick={() => {
                            dispatch(removeFromCart(selectedItem));
                            dispatch(closeModal());
                        }}
                        className="btn btn-danger w-100"
                    >
                        Remove
                    </button>
                ) : (
                    <button
                        onClick={() => {
                            dispatch(addToCart(selectedItem));
                            dispatch(closeModal());
                        }}
                        className="btn bg-success w-100"
                    >
                        Add to Card
                    </button>
                )}
            </div>
            <div className="modalBg" onClick={() => dispatch(closeModal())}></div>
        </>
    );
}

export { Modal };


