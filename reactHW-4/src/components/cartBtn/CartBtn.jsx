const CartBtn = (props) => {
    return (
        <button type="button" className="btn btn-lg pt-0 bg-dark-subtle position-relative">
            <img width={25} height={25} src="./img/shopping-cart.png" alt="shopping-cart"/>
            <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-success">
        {props.cartCount}
      </span>
        </button>
    );
}

export { CartBtn };

