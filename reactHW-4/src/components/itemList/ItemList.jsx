import { useSelector } from "react-redux";

import { ItemCard } from "../itemCard/ItemCard";

const ItemList = ({ isFav, isCart }) => {
    const items = useSelector((state) => state.items);
    const favItems = useSelector((state) => state.favItems);
    const cartItems = useSelector((state) => state.cartItems);

    return (
        <>
            {(isFav && favItems.length === 0) || (isCart && cartItems.length === 0) ? (
                <p className="text-center fs-4">No planes here...</p>
            ) : null}
            <div className="container p-0">
                <ul className="d-flex justify-content-center mt-4 p-0 flex-wrap gap-4">
                    {isCart
                        ? cartItems.map((item, index) => {
                            return <ItemCard inCart={true} key={index} item={item} />;
                        })
                        : null}
                    {isFav
                        ? favItems.map((item) => {
                            return <ItemCard favorite={true} key={item.id} item={item} />;
                        })
                        : null}
                    {!isFav && !isCart
                        ? items.map((item) => {
                            return (
                                <ItemCard
                                    favorite={favItems.find((favItem) => favItem.id === item.id) ? true : false}
                                    key={item.id}
                                    item={item}
                                />
                            );
                        })
                        : null}
                </ul>
            </div>
        </>
    );
}

export { ItemList };

