import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { AppRoutes } from "./AppRoutes";
import { Header } from "./components/header/Header";
import { Modal } from "./components/modal/Modal";
import { fetchItems } from "./redux/slices/itemsSlice";

function App() {
    const dispatch = useDispatch();
    const modalOpened = useSelector((state) => state.modal.open);

    useEffect(() => {
        dispatch(fetchItems());
    }, [dispatch]);

    return (
        <>
            <Header />
            <AppRoutes />
            {modalOpened ? <Modal /> : null}
        </>
    );
}
export default App;

