import { createSlice } from "@reduxjs/toolkit";

const favItemsSlice = createSlice({
    name: "favorites",
    initialState: [],
    reducers: {

        setFavItems: (state, action) => {
            return action.payload;
        },

        addToFav: (state, action) => {
            state.push(action.payload);
        },

        removeFromFav: (state, action) => {
            const newFavorites = state.filter((item) => item.id !== action.payload);
            return newFavorites;
        },
    },
});

export default favItemsSlice.reducer;

export const { addToFav, removeFromFav, setFavItems  } = favItemsSlice.actions;


