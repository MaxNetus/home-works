import { createSlice } from "@reduxjs/toolkit";

const cartItemsSlice = createSlice({
    name: "favorites",
    initialState: [],
    reducers: {
        setCartItems: (state, action) => {
            return action.payload;
        },

        addToCart: (state, action) => {
            state.push(action.payload);
        },

        removeFromCart: (state, action) => {
            const deleteElIndex = state.findIndex((item) => item.id === action.payload.id);
            const newCartArr = state.filter((item, index) => index !== deleteElIndex);
            return newCartArr;
        },
    },
});

export default cartItemsSlice.reducer;

export const { addToCart, removeFromCart, setCartItems } = cartItemsSlice.actions;

