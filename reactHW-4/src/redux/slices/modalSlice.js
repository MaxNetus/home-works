import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
    name: "modal",
    initialState: {
        open: false,
        isRemoving: false,
    },
    reducers: {
        openModal: (state, action) => {
            state.open = true;
            state.isRemoving = action.payload ? true : false;
        },

        closeModal: (state, action) => {
            state.open = false;
            state.isRemoving = false;
        },
    },
});

export default modalSlice.reducer;

export const { openModal, closeModal } = modalSlice.actions;


