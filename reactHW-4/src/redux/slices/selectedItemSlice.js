import { createSlice } from "@reduxjs/toolkit";

const selectedItemSlice = createSlice({
    name: "selected",
    initialState: null,
    reducers: {
        setSelectedItem: (state, action) => {
            return action.payload;
        },
    },
});

export default selectedItemSlice.reducer;

export const { setSelectedItem } = selectedItemSlice.actions;


