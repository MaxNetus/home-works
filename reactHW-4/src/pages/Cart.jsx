import { ItemList } from "../components/itemList/ItemList";

const Cart = () => {
    return (
        <>
            <h3 className="text-center pt-4">Items in your Cart:</h3>
            <ItemList isCart={true} />
        </>
    );
}

export { Cart };




