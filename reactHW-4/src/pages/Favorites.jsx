import { ItemList } from "../components/itemList/ItemList";

const Favorites = () => {
    return (
        <>
            <h3 className="text-center pt-4 pb-3">Your Favorites:</h3>
            <ItemList isFav={true} />
        </>
    );
}

export { Favorites };



