"use strict"

const colorChg = document.querySelector('.changeColor');
let curColl = document.querySelector('#color');

colorChg.addEventListener('click', changeColors);

function changeColors() {
  let cssLink = curColl.getAttribute('href');
  const originCol = './style.css';
  const altCol = './styleAlt.css';
  cssLink === originCol ? cssLink = altCol : cssLink = originCol;
  curColl.setAttribute('href', cssLink);
  localStorage.setItem('color', cssLink);
}

let storageColor = localStorage.getItem('color');
curColl.setAttribute('href', storageColor);

