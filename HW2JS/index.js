// 1. У JS існують такі типи данних: null, undefined, boolean, string, number, object, symbol, bigint.
// 2. == не суворе порівняння (може порівнювати данні без урахування їх типу), === суворе порівняння (порівнюються данні і їх тип).
// 3. Оператори в JS використовуються для управління потоком команд. 


let userName = prompt('What is your name?');
while (userName === "" || userName === null) {
  userName = prompt('What is your name?');
}

let age = prompt('How old are you?');
while (age === '' || age === null || !parseInt(age)) {
  userName = prompt('What is your name?', userName);
  age = prompt('How old are you?');
}

if (age < 18) {
  alert('You are not allowed to visit this website');
} else if (18 <= age && age <= 22) {
  let confirming = confirm('Are you sure you want to continue?');
  if (confirming === true) {
    alert(`Welcome, ${userName}`);
  } else {
    alert('You are not allowed to visit this website');
  }
} else if (age > 22) {
  alert(`Welcome, ${userName}`);
}



