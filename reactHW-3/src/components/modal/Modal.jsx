import "./modal.scss";

export function Modal({
                          selectedItem,
                          selectedItem: {name, price, img},
                          removingModalOpen,
                          addToCart,
                          removeFromCart,
                          closeModal,
                      }) {
    return (
        <>
            <div className="modal-bg" onClick={closeModal}></div>
            <div
                className="d-flex flex-column text-center rounded p-4 pt-3 position-fixed top-50 start-50 translate-middle bg-secondary-subtle fs-4"
                style={{zIndex: 2, width: "400px"}}
            >
                <div className="d-flex justify-content-between align-items-center mb-2">
                    <p className="m-0 fs-5">
                        {removingModalOpen ? `Remove from cart:` : `Add to a Cart:`}
                    </p>
                    <button onClick={closeModal} className="btn p-0 fw-bold fs-4 border-0">
                        &#10005;
                    </button>
                </div>
                <h3>{name}</h3>
                <img src={img} width={"100%"} alt="plane"/>
                <p className="fw-bold mt-2">{"Price: " + price + "₴"}</p>
                {removingModalOpen ? (
                    <button
                        onClick={() => {
                            removeFromCart(selectedItem);
                            closeModal();
                        }}
                        className="btn btn-danger w-100"
                    >
                        Remove
                    </button>
                ) : (
                    <button
                        onClick={() => {
                            addToCart(selectedItem);
                            closeModal();
                        }}
                        className="btn bg-success w-100"
                    >
                        Add to Card
                    </button>
                )}
            </div>
        </>
    );
}
