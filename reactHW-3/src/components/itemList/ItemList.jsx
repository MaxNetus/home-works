import { ItemCard } from "../itemCard/ItemCard";

const ItemList = ({ items, favItems, cartItems, openModal, addToFav, removeFromFav }) => {
    return (
        <>
            <div className="container p-0">
                <ul className="d-flex justify-content-center mt-4 p-0 flex-wrap gap-4">
                    {cartItems
                        ? cartItems.map((item, index) => {
                            return <ItemCard key={index} cartItem={true} openModal={openModal} item={item} />;
                        })
                        : items.map((item) => {
                            return (
                                <ItemCard
                                    openModal={openModal}
                                    key={item.id}
                                    addToFav={addToFav}
                                    removeFromFav={removeFromFav}
                                    item={item}
                                    favorite={favItems.find((favItem) => favItem.id === item.id) ? true : false}
                                />
                            );
                        })}
                </ul>
            </div>
        </>
    );
}

export { ItemList }
