const ItemCard = ({
                      favorite,
                      addToFav,
                      removeFromFav,
                      openModal,
                      cartItem,
                      item: {id, name, img, price, color, article},
                  }) => {
    function favBtnClick(e, id) {
        favorite ? removeFromFav(e, id) : addToFav(e, id);
    }

    return (
        <>
            <li id={id} className="card bg-secondary-subtle" style={{width: "350px"}}>
                {cartItem ? (
                    <button
                        onClick={(e) => openModal(e, id, true)}
                        className="align-self-end btn p-0 me-3 mt-1 fw-bold fs-4 border-0"
                    >
                        &#10005;
                    </button>
                ) : null}
                <img src={img} className="card-img-top p-3" alt="item-img"/>
                <div className="card-body d-flex flex-column justify-content-end">
                    <h3 className="card-title">{name}</h3>
                    <p className="card-text">{"Article: " + article}</p>
                    <p className="card-text">{"Color: " + color}</p>
                    <p className="card-text fw-bold">{"Price: " + price + "₴"}</p>
                    {cartItem ? null : (
                        <div className="d-flex justify-content-between">
                            <button onClick={(e) => favBtnClick(e, id)} className="btn p-0 border-0">
                                <img width={25} height={25} src={favorite ? "./img/star-active.png" : "./img/star.png"}
                                     alt="star"/>
                            </button>
                            <button onClick={(e) => openModal(e, id)} className="btn bg-info">
                                Add to Cart
                            </button>
                        </div>
                    )}
                </div>
            </li>
        </>
    );
}

export { ItemCard };
