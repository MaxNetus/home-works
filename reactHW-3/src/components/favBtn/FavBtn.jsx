
const FavoritesBtn = (props) => {
    return (
        <button type="button" className="btn btn-secondary btn-lg position-relative pt-0">
            <img width={25} height={25} src="./img/star-active.png" alt="favorite-star" />
            <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-warning">
        {props.favCount}
      </span>
        </button>
    );
}

export { FavoritesBtn }
