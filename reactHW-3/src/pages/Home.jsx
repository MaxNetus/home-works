import { ItemList } from "../components/itemList/ItemList";

const Home = ({ items, favItems, addToFav, removeFromFav, openModal }) => {
    return (
        <>
            <ItemList
                items={items}
                openModal={openModal}
                addToFav={addToFav}
                removeFromFav={removeFromFav}
                favItems={favItems}
            />
        </>
    );
}

export { Home };
