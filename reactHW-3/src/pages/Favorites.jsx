import { ItemList } from "../components/itemList/ItemList";

const Favorites = ({ items, favItems, addToFav, removeFromFav, openModal }) => {
    return (
        <>
            {favItems.length === 0 ? <h3 className="text-center pt-4 pb-3">No items here...</h3> : <h3 className="text-center pt-4 pb-3">Your Favorite items:</h3>}
            <ItemList
                items={items}
                openModal={openModal}
                addToFav={addToFav}
                removeFromFav={removeFromFav}
                favItems={favItems}
            />
        </>
    );
}

export { Favorites };
