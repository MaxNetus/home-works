import {ItemList} from "../components/itemList/ItemList";

const Cart = (props) => {
    return (
        <>
            {props.cartItems.length === 0 ? <h3 className="text-center  pt-4 pb-2">No items here...</h3> :
                <h3 className="text-center pt-4">Items in your Cart:</h3>}}
            <ItemList openModal={props.openModal} cartItems={props.cartItems}/>
        </>
    );
}

export { Cart };

