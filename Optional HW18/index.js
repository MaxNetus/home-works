"use strict"

// Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
// В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.


const car = {
  name: 'toyota',
  model: 'Raw4',
  engine: 2.4,
  system: {
    ABS: 'plus',
    ABD: 'active',
    options: {
      whealHeater: 'yes',
      panorama: 'no',
      eclology: [1, 2, 3, [5, 6, 7], 4, 5],
    },
  },
}


function getClone(obj) {
  let clone = {};
  for (let key in obj) {
    if (typeof obj[key] === 'object') {
      getClone(obj[key]);
    }
    clone[key] = obj[key];
  }
  return clone;
}

const res = getClone(car);
console.log(res);












