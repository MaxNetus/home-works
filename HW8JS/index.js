// DOM це інтерфейс який дозволяє отримати доступ до html сторінки, а також змінювати її.
// innerText покаже нам увесь текст між відкриваючим та закриваючим тегом html. Якщо всередині innerText будуть ще якісь єлементи html зі своїм вмістом,
// то ці елементи будуть проігноровані і повернеться лише внутрішній текст. innerHTML поверне увесь текст а також внутрішню розмітку між відкриваючим та закриваючим тегами.
// Звернутися до елемента сторінки за допомогою JS можна querySelector, getElementById, getElementsByTagName та getElementsByClassName. Найсучасніший, найпотужніший та поширеніший 
// метод querySelector, тому що він універсальний.

"use strict"

// 1
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(i => i.style.backgroundColor = '#ff0000');
console.log(paragraphs);


// 2
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionList = document.querySelector('#optionsList');
console.log(optionList);
console.log(optionList.parentElement);
if(optionList.hasChildNodes()) {
  optionList.childNodes.forEach(i => console.log(`Node Name - ${i.nodeName} Node Type: ${i.nodeType}`))
}


// 3
// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';
console.log(testParagraph);


// 4
// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header');
console.log(mainHeader.children);
const allChild = [...mainHeader.children];
console.log(allChild);
allChild.forEach(i => i.classList.add('nav-item'));
console.log(allChild);


// 5
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(i => i.classList.remove('section-title'));
console.log(sectionTitle);













