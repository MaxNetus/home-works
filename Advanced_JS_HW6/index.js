// Асинхронність у Javascript - це можливість працювати над декількома задачами одночасно. Але це все залежить від
// задачі. Наприклад, якщо у нас є 2 різні функції, які не залежать одна від одної (вони відправляють якісь запити на
// сервер). То в такому випадку ці запити будуть виконуватися асинхронно, не залежно одна від одної. А якщо є 2 функції
// і результат 2-гої залежить від 1-ї, то нам треба буде почекати щоб спочатку відпрацювала 1-ша фукція, а вже потоім
// почне свою роботу друга.

const btn = document.querySelector('.btn');
const info = document.querySelector('.info');
const loader = document.querySelector('.loader-wrap');

btn.addEventListener('click', letsGetStarted, {once: true});

async function getIp() {
    try {
        const requestIP = await fetch('https://api.ipify.org/?format=json');
        const {ip} = await requestIP.json();
        return ip
    } catch (e) {
        throw new Error(e.message);
    }
}

async function getAddress(ip) {
    try {
        const requestAddress = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`);
        const {continent, country, regionName, city, district} = await requestAddress.json();
        return {continent, country, regionName, city, district};
    } catch (e) {
        throw new Error(e.message);
    }
}

async function showAddress({continent, country, regionName, city, district}) {
    try {
        const address = document.createElement('div');
        address.classList.add('address');
        address.innerHTML = `
            <h2> Current location of requested ID:  </h2>
            <p> Continent: ${continent} </p>
            <p> Country: ${country} </p>
            <p> Region: ${regionName} </p>
            <p> City: ${city} </p>
<!--            <p> District: ${district} </p>-->
`;
        info.append(address)
    } catch (e) {
        throw new Error(e.message);
    }
}

async function letsGetStarted() {
    try {
        startLoading();
        const ip = await getIp();
        const {continent, country, regionName, city, district} = await getAddress(ip);
        const showFullInfo = await showAddress({continent, country, regionName, city, district});
        endLoad();
        return showFullInfo;
    } catch (e) {
        throw new Error(e.message);
    }
}



function startLoading() {
    loader.classList.add('active');
}

function endLoad() {
    loader.classList.remove('active');
}























// async function requestUserLocation() {
//     try {
//         const requestIP = await fetch('https://api.ipify.org/?format=json');
//         const {ip} = await requestIP.json();
//
//         const requestAddress = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`);
//         const {continent, country, regionName, city, district} = await requestAddress.json();
//
//         const address = document.createElement('div');
//         address.classList.add('address');
//         address.innerHTML = `
//             <h2> Current location of requested ID:  </h2>
//             <p> Continent: ${continent} </p>
//             <p> Country: ${country} </p>
//             <p> Region: ${regionName} </p>
//             <p> City: ${city} </p>
// <!--            <p> District: ${district} </p>-->
// `;
//         info.append(address)
//     } catch (err) {
//         throw new Error(err)
//     }
// }


