// Фунцкії потрібні щоб використовувати один і той самий код декілька разів з будь якого місця скрипта. Це значно 
// спрощує код, робить його менш навантаженим, а значить розробнику буде простіше в ньому орієнтуватись.
// Аргументи можуть змінюватися у скрипті багато разів, вводяться нові дані. Кожного разу як вводяться нові аргументи,
// функція обчислює новий результат.
// Return завершує виконання даної функції і повертає її значення. В подальшому ці значення можуть бути використані 
// як завгодно (виведенням в консоль, для подальших обчислень і т.п.)




function getResult() {
  let frsNumb = +prompt('Enter first number please');
  while (frsNumb === '' || frsNumb === null || !parseInt(frsNumb)) {
    frsNumb = prompt('Enter first number please', frsNumb)
  }

  let secNumb = +prompt('Enter second number please');
  while (secNumb === '' || secNumb === null || !parseInt(secNumb)) {
    frsNumb = prompt('Enter first number please', frsNumb)
    secNumb = prompt('Enter second number please', secNumb)
  }

  let option = prompt('Chose an option you need: +, -, *, /');
  while (option != '+' && option != '-' && option != '*' && option != '/') {
    option = prompt('Chose an option you need: +, -, *, /', option)
  }

  let res;

  switch (option) {
    case '+': {
      res = frsNumb + secNumb;
      break;
    }
    case '-': {
      res = frsNumb - secNumb;
      break
    }
    case '*': {
      res = frsNumb * secNumb;
      break
    }
    default: {
      res = frsNumb / secNumb;
    }
  }

  return res;
}

console.log(getResult());

