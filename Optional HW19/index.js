"use strict"

// Реалізувати функцію, яка дозволить оцінити, чи команда розробників встигне здати проект до настання дедлайну. Завдання має бути виконане на чистому Javascript без використання
// бібліотек типу jQuery або React.

// Технічні вимоги:
// Функція на вхід приймає три параметри:
// масив із чисел, що становить швидкість роботи різних членів команди. Кількість елементів у масиві означає кількість людей у команді. Кожен елемент означає скільки стор поінтів
//  (умовна оцінка складності виконання завдання) може виконати даний розробник за 1 день.
// масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати). Кількість елементів у масиві означає кількість завдань у беклозі. Кожне число в масиві означає кількість 
// сторі поінтів, необхідні виконання даного завдання.
// Дата дедлайну (об'єкт типу Date).
// Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня). 
// Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ? днів до настання дедлайну!. Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення
//  Команді розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
// Робота триває по 8 годин на день по будніх днях



function checkIfDoneBeforeDeadline (Team, Backlog, deadline ) {

    let teamStoryPointperDay = Team.reduce((a, b) => a + b);
    let totalBacklog = Backlog.reduce((a, b) => a + b);
    let daysNeeded = Math.ceil(totalBacklog / teamStoryPointperDay)

    let daysAvailable = getWorkingDays(deadline).length;
    if(daysAvailable - daysNeeded > 0) {
        console.log(`Усі завдання будуть успішно виконані за ${daysAvailable - daysNeeded} днів до настання дедлайну!`);}
     else { console.log(`Команді розробників доведеться витратити додатково ${(daysNeeded - daysAvailable)*8} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }

}

/* робимо додаткову функцію, яка буде враховувати лише робочі дні від сьогоднішнього дня до дейдайлу. Буде повертати результат в вигляді масиву з робочими днями. */

function getWorkingDays(deadl) {
    let arr = [];
    let todayDay = new Date();
    let deadlineDay = new Date(deadl);
    let daysPeriod = Math.ceil((deadlineDay - todayDay)/86400000);

    for( let i = todayDay.getDay(); i <= (daysPeriod + todayDay.getDay()); i++) {
    arr.push(i)
    }

    let weeks = [];
    let newArr2 = [];
    let size = 7;
    
    for (let i = 1; i < todayDay.getDay(); i++) {
        arr.unshift(0);
    }

    for (let i = 0; i < Math.ceil(arr.length/size); i++) {
        newArr2.push(weeks[i] = arr.slice((i*size), (i*size) + size));
    }
    
    let arrWorkDays = newArr2.map((value) => value.filter((value, index) => index < 5));
    let workingDays = arrWorkDays.flat().filter(value => value > 0);

    return workingDays;
}
    

checkIfDoneBeforeDeadline([10, 10, 10, 10], [50, 50, 50, 1000, 50, 100], "2022.12.29")
checkIfDoneBeforeDeadline([10, 10, 10, 10], [50, 50, 50, 50, 100], "2024.01.10")








