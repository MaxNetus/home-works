
// // Метод об'єкту це функція всередині об'єкту.
// Будь який тип данних може мати значення властивості об'єкта. Це залежить від того які данні ми туди пропишимо. 
// Це означає що так просто ми не можемо скопіювати обєкт. Буде скопійоване тільки посилання на нього. Всі дії які ми будеми робити будуть змінювати значення які є в цьому обєкті.

"use strict"


function createNewUser() {

  const newUser = {
    firstName: prompt('Enter your name'),
    lastName: prompt('Enter your last name'),
    age: +prompt('How old are you?'),
    getLogin: function () {
      console.log((this.firstName[0] + this.lastName).toLowerCase());
    },
  }

  Object.defineProperty(newUser, 'firstName', {
    writable: false,
  });
  Object.defineProperty(newUser, 'lastName', {
    writable: false,
  });
  return newUser;
};

let user = createNewUser();
// user.firstName = "Tobias";
// user.lastName = "Alderweireld";
user.getLogin();
console.log(user);





