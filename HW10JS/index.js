


"use strict"

const btn = document.querySelector('.tabs');
const item = document.querySelectorAll('.item');
const tabs = document.querySelectorAll('.tabs-title')

btn.addEventListener('click', function (e) {
  tabs.forEach(i => {
    i.classList.remove('active');
  })
  if (e.target.classList.contains('tabs-title')) {
    e.target.classList.toggle('active')
  };
  item.forEach(i => {
    i.classList.remove('item-active');
  })
  const id = e.target.getAttribute('data-tab');
  document.querySelector(id).classList.add('item-active');

  
})



