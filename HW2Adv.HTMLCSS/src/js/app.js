const menuBtn = document.querySelector(".header__burger");
const menuShow = document.querySelector('.header__hiden-menu');

menuBtn.addEventListener("click", (e) => {
  const target = e.target.closest("button");
  for (const i of target.children) {
    i.classList.toggle("header__burger--hidden");
  }
  menuShow.classList.toggle('menuShow');
})


