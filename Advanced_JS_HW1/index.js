// Прототипне наслідування в Javascript працює шляхом наслідування дочірнім елементом властивостей та методів батька.
    // Таких вложеностей може бути безліч.

// super() потрібно викликати щоб викликти функцію яка належить батьківському обєкту.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    set salary(value) {
        this._salary = value;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }
    set lang(value) {
        this._lang = value;
    }

}

const person1 = new Programmer("Max", 33, "2000", ["Python", "C++"]);
const person2 = new Programmer("Ahmed", 45, "1500",  ["JS", "Python"]);
// console.log(person1);
// person1.name = 'Vasyl';
// person1.lang = ['C++', 'JAVA']
// console.log(person1);

// console.log(person2);
// person2.age = 55;
// console.log(person2.salary);
// console.log(person2);