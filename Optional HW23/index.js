"use strict"

// Обробник подій викликає функцію коли виконується певна дія (клік миші, скрол, натискання на певну клавішу,...)
// Додати обробник подій можна через onclick або addEventListener. Кращий спосіб - це addEventListener, тому що він більш функціональний та дозводяє на один елемент повісити декілька подій.

// Завдання
// Створити поле для введення ціни з валідацією. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// При завантаженні сторінки показати користувачеві поле вводу (input) з написом Price. Це поле буде служити для введення числових значень
// Поведінка поля має бути наступною:
// При фокусі на полі вводу - має з'явитися рамка зеленого кольору. У разі втрати фокусу вона пропадає.
// Коли прибрано фокус з поля - його значення зчитується, над полем створюється span, у якому має бути текст: Поточна ціна: ${значення з поля вводу}. Поруч із ним має бути кнопка з 
// хрестиком (X). Значення всередині поля вводу забарвлюється у зелений колір.
// При натисканні на Х - span з текстом та кнопка X повинні бути видалені. Значення, введене у поле вводу, обнулюється.
// Якщо користувач ввів число менше 0 – при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу – Please enter correct price. span з некорректним значенням 
// при цьому не створюється. У папці img лежать приклади реалізації поля вводу та "span", які можна брати як приклад

const input = document.querySelector('.input');
const span = document.querySelector('.span');
const action = document.querySelector('.action');
const img = document.querySelector('.img');
const wrong = document.querySelector('.wrong');

input.addEventListener('focus', function (ev) {
  input.classList.add('focus');
})
input.addEventListener('blur', function (ev) {
  let value = +input.value;
  if (isNaN(input.value)) {
    input.value = '';
  }
  if (input.value < '0') {
    input.style.border = '5px solid red';
    wrong.style.display = 'block';
  } else {
    wrong.style.display = 'none';
    input.style.border = '5px solid green';
    input.classList.remove('focus');
    input.style.color = 'green';
    action.style.display = 'block';
    span.textContent = `Поточна ціна: ${input.value} Uah`;
  }
})
img.addEventListener('click', function (ev) {
  action.style.display = 'none';
  input.value = '';
})

