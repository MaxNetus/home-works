// AJAX - асинхронний JS і XML. За допомогою цієї технології можна взаємодіяти з сервером не перезавантажуючи сторінку
// браузера. Сучасний метод роботи з сервером, який значно полегшує життя розробнику.


function getFilm(url) {
    return fetch(url)
        .then(response => response.json());
}

function createList(array) {
    const newList = document.createElement("ul");
    array.forEach(elem => {
        newList.insertAdjacentHTML("beforeend", `<li>${elem}</li>`);
    });
    return newList;
}

function getActors(arr) {
    arr.forEach(({ characters }, idx) => {
        const charsPromise = characters.map(actor => {
            return fetch(actor)
                .then(response => response.json())
                .then(({ name }) => name);
        });

        Promise.allSettled(charsPromise)
            .then(resolve => {
                return resolve.map(({ value }) => value);
            })
            .then(charArr => {
                const li = document.querySelector("ul");
                li.children[idx].append(createList(charArr));
            });
    });
}

getFilm("https://ajax.test-danit.com/api/swapi/films")
    .then(filmArr => {
        const filmsList = createList(
            filmArr.map(({ name, episodeId, openingCrawl }) => {
                return `<b>Episode ${episodeId}</b> <br>
          <b>${name}</b> <br>
          ${openingCrawl} <br>
          <b>Actors:</b>`;
            })
        );
        document.body.append(filmsList);
        return filmArr;
    })
    .then(filmList => {
        getActors(filmList);
    })
    .catch(error => {
        console.error(error);
    });
