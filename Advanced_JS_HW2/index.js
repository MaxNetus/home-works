// конструкцію try...catch доречно використовувати наприклад, коли ми отримуємо обєкт з серверу і нам потрібно
// вивести якусь властивість, але ми не впевнені що така властивість є.
//     Або коли ми очікуємо якісь дані від користувача, потім проводимо верифікацію і якщо користувач ввів некоректні дані
// виводимо помилку.

const div = document.querySelector('#root');
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    },
];

function createListBooks(arr) {
    const ul = document.createElement('ul');
    for (let i of arr) {

        try {
            if (!i.name) {
                throw new SyntaxError('Немає імені!');
            } else if (!i.author) {
                throw new SyntaxError('Немає автора!');
            } else if (!i.price) {
                throw new SyntaxError('Немає ціни!');
            } else {
                let book = document.createElement('li');
                book.innerText = `${i.name}, ${i.author}, ${i.price}`;
                ul.append(book);
            }
        } catch (e) {
            console.log(`У книзі '${i.name}' помилка: '${e.message}'`);
        }
    }
    div.append(ul);
}

createListBooks(books);