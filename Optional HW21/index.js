"use strict"

const btn = document.querySelector('.btn');
btn.addEventListener('click', function (ev) {
  let diam = +prompt('Введіть діаметр(px)');
  while (isNaN(diam) || diam === 0) {
    diam = +prompt('Введіть діаметр(px)')
  }
  const create = document.createElement('button');
  create.textContent = 'Намалювати';
  create.classList.add('btn');
  document.body.append(create);
  create.addEventListener('click', function (ev) {
    for (let i = 0; i < 100; i++) {
      const circle = document.createElement('div');
      circle.style.cssText = `
   width: ${diam}px;
   height: ${diam}px;
   border-radius: 50%;
   margin: 5px;
   display: inline-block;
   `
      circle.style.backgroundColor = getRandomColor();
      create.after(circle);
      circle.classList.add('circle');
    }
  })
  document.body.addEventListener('click', function (e) {
    if (e.target.classList.contains('circle')) {
      e.target.remove();
    }
  })
})

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}