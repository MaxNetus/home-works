const modals = [
    {
        id: 1,
        className: "modal first-modal",
        headerText: "First Modal Window",
        closeButton: false,
        text: "Are you sure you want to close?",
        buttons: {
            btn1: {
                text: "ok",
            },
            btn2: {
                text: "cancel",
            },
        },
    },
    {
        id: 2,
        className: "modal second-modal",
        headerText: "Second Modal Window",
        closeButton: true,
        text: "Press to close the Modal Window",
        buttons: {
            btn1: {
                text: "ok",
            },
            btn2: {
                text: "no",
            },
        },
    },
];

export { modals }
