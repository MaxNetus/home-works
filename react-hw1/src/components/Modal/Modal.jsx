import { Component } from "react";
import { Button } from "../button/Button";
import { modals } from "./modalsList";
import "./modal.scss";

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = { modalInfo: modals.find((modal) => modal.id === +props.modalId) };
    }
    render() {
        const { className, headerText, closeButton, buttons, text } = this.state.modalInfo;
        return (
            <>
                <div className="modalWrap">
                    <div className={className}>
                        <header className="modal-header">
                            {headerText}
                            {closeButton ? (
                                <button onClick={this.props.closeFn} className="closeBtn">
                                    &#10006;
                                </button>
                            ) : null}
                        </header>
                        <p>{text}</p>
                        <div className="modalBtnWrapper">
                            <Button className="modalBtn" text={buttons.btn1.text} onClickFn={this.props.closeFn} />
                            <Button className="modalBtn" text={buttons.btn2.text} onClickFn={this.props.closeFn} />
                        </div>
                    </div>
                </div>
                <div className="modalBg" onClick={this.props.closeFn}></div>
            </>
        );
    }
}

export { Modal }
