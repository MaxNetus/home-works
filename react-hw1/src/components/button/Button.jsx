import { Component } from "react";

import "./button.scss";

class Button extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <button
                data-modal-id={this.props.id}
                className={this.props.className}
                style={{ backgroundColor: this.props.backgroundColor }}
                onClick={this.props.onClickFn}
            >
                {this.props.text}
            </button>
        );
    }
}

export { Button }
