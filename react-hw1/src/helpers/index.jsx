import { Component } from "react";
import { Button } from "../components/button/Button";
import { Modal } from "../components/modal/Modal";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = { modalOpened: false, openedModalId: 0 };
    }
    openModal = (e) => {
        this.setState({ modalOpened: true, openedModalId: +e.target.dataset.modalId });
    };
    closeModal = () => {
        this.setState({ modalOpened: false, openedModalId: 0 });
    };
    render() {
        return (
            <>
                <div className="btnWrap">
                    <Button
                        id="1"
                        className="btn"
                        text="First Modal"
                        backgroundColor="LightSalmon"
                        onClickFn={this.openModal}
                    />
                    <Button
                        id="2"
                        className="btn"
                        text="Second Modal"
                        backgroundColor="#DDA0DD"
                        onClickFn={this.openModal}
                    />
                </div>
                {this.state.modalOpened ? <Modal modalId={this.state.openedModalId} closeFn={this.closeModal} /> : null}
            </>
        );
    }
}

export { Index }
