const root = document.querySelector('.root');
const userURL = 'https://ajax.test-danit.com/api/json/users';
const postURL = 'https://ajax.test-danit.com/api/json/posts';

document.addEventListener('click', removeCard);

class Card {
    constructor(userURL, postURL) {
        this.userURL = userURL;
        this.postURL = postURL;
    }

    async createTwits() {
        try {
            const userResponse = await fetch(this.userURL);
            const postResponse = await fetch(this.postURL);
            const users = await userResponse.json();
            const result = await postResponse.json();
            const postsArr = await result.map(({id, userId, title, body}) => {
                const div = document.createElement('div');
                div.classList.add('userDiv', `card-${id}`);
                div.innerHTML = `
        <p class="userName"> User: ${users.find(user => user.id === userId).name} </p>
        <p class="userEmail"> E-mail: ${users.find(user => user.id === userId).email} </p>
        <p class="title"> Title: ${title} </p>
        <p class="body"> Post: ${body} </p>
        <button class="removeCard" data-#=${id}>Delete</button>
        `;
                root.append(div);
            })
            return await postsArr;
        } catch (err) {
            console.log(`Sorry, error: ${err}`)
        }
    }
}

async function removeCard(e) {
    if (e.target.classList.contains('removeCard')) {
        let id = e.target.getAttribute('data-#');

        try {
            const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                method: 'DELETE',
                headers: {
                    "Content-Type": "application/json",
                }
            })

            if (result.status === 200) {
                const cardToRemove = document.querySelector(`.card-${id}`);
                cardToRemove.remove()
            }
        } catch (err) {
            throw new Error(err);
        }

    }
}

const newCard = new Card(userURL, postURL)
newCard.createTwits();








