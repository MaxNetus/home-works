// Екранування використовується коли ми хочемо прописати спец символ як звичайний. Для цього перед символом нам потрібно поставити\
// Функцію оголосити можна через function expression та function declaration. Функція всередині обєкта називається метод. 
// hoisting це механізм в JS при якому змінні та функції можуть бути перемцщені вгору по коду (тобто ще перед тим як вони будуть виконані)


"use strict"


function createNewUser() {

  const newUser = {
    firstName: prompt('Enter your name'),
    lastName: prompt('Enter your last name'),
    birthday: prompt('Enter your birth date "dd.mm.yyyy"'),
    getLogin: function () {
      console.log((this.firstName[0] + this.lastName).toLowerCase());
    },
    getPassword: function () {
      console.log(this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthYear.getFullYear());
    },
    getAge: function () {
      let age = new Date().getFullYear() - birthYear.getFullYear();
      console.log(age);
    },
  }
  let birthYear = new Date(newUser.birthday.split(".").reverse().join("."));
  Object.defineProperty(newUser, 'firstName', {
    writable: false,
  });
  Object.defineProperty(newUser, 'lastName', {
    writable: false,
  });
  return newUser;
};

let user = createNewUser();
// user.firstName = "Tobias";
// user.lastName = "Alderweireld";
user.getLogin();
user.getPassword();
user.getAge();
console.log(user);


















